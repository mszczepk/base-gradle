package mszczepk

import kotlin.test.Test
import org.amshove.kluent.shouldBeEqualTo

internal class ATest {

   @Test
   fun `should test A`() {
      A.str() shouldBeEqualTo "A"
   }
}
