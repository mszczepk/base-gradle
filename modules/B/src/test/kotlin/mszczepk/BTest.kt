package mszczepk

import org.amshove.kluent.shouldBeEqualTo
import kotlin.test.Test

internal class BTest {

   @Test
   fun `should test B`() {
      B.useA() shouldBeEqualTo "B + A"
   }
}
