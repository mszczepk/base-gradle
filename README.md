Base Gradle
===========
Basic project setup built with Gradle.


Setup
-----
1. `git clone https://gitlab.com/mszczepk/base-gradle.git`
2. `mv base-gradle <PROJECT>`
3. `cd <PROJECT>`
4. `rm -rf .git`
5. `sed -i 's/base-gradle/<PROJECT>/g' settings.gradle`
